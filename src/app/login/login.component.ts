import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';

import { LoggerService } from '../services/logger/logger.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUser: any;
  loginForm: FormGroup;
  isSubmitted = false;
  constructor(private fb: FormBuilder, public loggerservice: LoggerService, private router: Router) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  // login handler
  loginHandler() {
    if (this.loginForm.valid) {
      this.isSubmitted = true;
      // this.loggerservice.log("login successfully");
      this.router.navigate(['home']);
    }
    this.isSubmitted = false;
    const logindetails = this.loginForm.value;
    localStorage.setItem('currentUser', logindetails.username);
  }

  ngOnInit(): void {
  }

}
