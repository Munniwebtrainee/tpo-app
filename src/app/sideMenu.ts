export const menuItems = [
    {
        title: 'Planning',
        routerLink: 'planning',
        icon: 'timeline',
        subMenu: [
            {
                title: 'Promotion',
                routerLink: 'planning/promotion'
            },
            {
                title: 'Volume',
                routerLink: 'planning/volume'
            }
        ]
    },

]
