import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';

const routes: Routes = [
    {
        path: '', component: HomeComponent,
        children: [

            {
                path: 'planning',
                loadChildren: () => import('../home/planning/planning.module').then(m => m.PlanningModule),
            },
            {
                path: 'reports',
                loadChildren: () => import('../home/reports/reports.module').then(m => m.ReportsModule),
            },

        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
