import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { PlanningModule } from './planning/planning.module';
import { ReportsModule } from './reports/reports.module';
// import { MaterialModule } from '../../app/material.module';


@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    PlanningModule,
    ReportsModule,
    // MaterialModule
  ]
})
export class HomeModule { }
