import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { PromotionComponent } from './promotion/promotion.component';
import { VolumeComponent } from './volume/volume.component';
import { MaterialModule } from '../../material.module';
import { PlanningRoutingModule } from '../planning/planning-routing.module';
import { PlanningComponent } from '../planning/planning.component';



@NgModule({
    declarations: [
        PlanningComponent,
        PromotionComponent,
        VolumeComponent,
    ],
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        PlanningRoutingModule,
    ]
})
export class PlanningModule { }
