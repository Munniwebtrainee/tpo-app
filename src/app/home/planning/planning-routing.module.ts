import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlanningComponent } from '../planning/planning.component';
import { PromotionComponent } from '../planning/promotion/promotion.component';
import { VolumeComponent } from '../planning/volume/volume.component';

const routes: Routes = [
    {
        path: '', component: PlanningComponent,

        children: [
            { path: 'promotion', component: PromotionComponent },
            { path: 'volume', component: VolumeComponent }
        ]

    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PlanningRoutingModule { }
