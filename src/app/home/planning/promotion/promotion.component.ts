import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';



interface FoodNode {
  name: string;
  children?: FoodNode[];
}
interface LocationNode {
  name: string;
  children?: LocationNode[];
}

interface TodoItemFlatNode {
  level: number;
  name: string;
  expandable: boolean;
}

interface TodoItemLocationFlatNode {
  level: number;
  name: string;
  expandable: boolean;
}

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.css']
})

export class PromotionComponent implements OnInit {
  selectPanel: any;
  arrProduct: any = [];
  arrLocation: any = [];
  arrCity: any = [];
  arrProdCategory: any = [];
  weeks: any;
  products: any;
  locationList: any;
  startDate: any;
  endDate: any;
  location: any = {};
  product: any = {};
  locationOutput: any = [];
  productOutput: any = [];
  userArray: any = [];
  arrTpr: any = [];
  arrFeature: any = [];
  arrDisplay: any = [];
  arrFeaDis: any = [];
  arrDiscount: any = [];
  selectedItems: any = [];
  interests = []; bChecked: any; wChecked: any; oChecked: any;
  checklistSelection = new SelectionModel<TodoItemFlatNode>(true)
  checkListSelection = new SelectionModel<TodoItemLocationFlatNode>(true)
  isChecked: boolean = false;
  mainForm: FormGroup;
  sliderForm: FormGroup;
  isSubmitted: boolean = false;

  private _transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  }
  private _nodeTransformer = (node: LocationNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  }
  treeControl = new FlatTreeControl<TodoItemFlatNode>(
    node => node.level, node => node.expandable);
  treeControlLocation = new FlatTreeControl<TodoItemLocationFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);
  treeFlattenerLocation = new MatTreeFlattener(
    this._nodeTransformer, node => node.level, node => node.expandable, node => node.children);

  dataSourceProducts = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  dataSourceLocation = new MatTreeFlatDataSource(this.treeControlLocation, this.treeFlattenerLocation);

  // products = new FormControl();
  // location = new FormControl();


  constructor(formBuilder: FormBuilder, public router: Router, public http: HttpClient) {
    // this.dataSource.data = TREE_DATA;
    // this.dataSource1.data = TREE_DATA1;
    this.sliderForm = formBuilder.group({
      tpr: false,
      feature: false,
      display: false,
      feadis: false,
      discount: false,
      discountValue: ''
    });
    this.mainForm = formBuilder.group({
      products: new FormControl('', Validators.required),
      locationList: new FormControl('', Validators.required),
      weeks: new FormControl(null, Validators.required),
      range: new FormGroup({
        start: new FormControl(),
        end: new FormControl()
      })
    })
    this.http.get('assets/data/sample_data.csv', { responseType: 'text' })
      .subscribe(data => {
        let csvToRowArray = data.split('\n');
        let rowCount = 1;
        csvToRowArray.forEach(e => {
          var newDataArr = e.split(",")
          this.userArray[this.userArray.length] = { Date: newDataArr[0], Product: newDataArr[1], City: newDataArr[2], Location: newDataArr[3], Baseline_Volume: newDataArr[4], TPR: newDataArr[5], Display_Only: newDataArr[6], Feature_Only: newDataArr[7], Fea_and_Disp: newDataArr[8], Price_Discount: newDataArr[10], Total_Volume: newDataArr[12], Product_Category: newDataArr[13] }
          if (rowCount == csvToRowArray.length) {
            for (var i = 0; i < this.userArray.length; i++) {
              this.arrProduct.push(this.userArray[i].Product);
              this.arrLocation.push(this.userArray[i].Location);
              this.arrCity.push(this.userArray[i].City);
              this.arrProdCategory.push(this.userArray[i].Product_Category);
              this.arrTpr.push(this.userArray[i].TPR);
              // console.log(this.arrTpr);
              this.arrFeature.push(this.userArray[i].Feature_Only)
              // console.log(this.arrFeature);
              if (i == this.userArray.length - 1) {
              }
            }
          }
          else {
            rowCount++;
          }

        });
        let city = this.arrCity;
        let locations = this.arrLocation;
        for (let i = 1; i < city.length; i++) {
          if (this.location[city[i]] && locations[i]) {
            this.location[city[i]].add(locations[i])
          }
          else if (city[i] && locations[i]) {
            let mySet = new Set();
            this.location[city[i]] = mySet.add(locations[i])
          }
        }
        for (let i in this.location) {
          let inner_parent: any = {};
          inner_parent['name'] = i;
          let child_array = [];
          // console.log(this.location[i])
          for (let j in Array.from(this.location[i].values())) {
            let inner_child: any = {};
            inner_child['name'] = Array.from(this.location[i].values())[j];
            child_array.push(inner_child)
          }
          inner_parent['children'] = child_array
          this.locationOutput.push(inner_parent)
        }
        this.dataSourceLocation.data = this.locationOutput;
        // console.log(output);
        // console.log(this.location);

        let prodCategory = this.arrProdCategory;
        let products = this.arrProduct;
        for (let k = 1; k < prodCategory.length; k++) {
          if (this.product[prodCategory[k]] && products[k]) {
            this.product[prodCategory[k]].add(products[k])
          }
          else if (prodCategory[k] && products[k]) {
            let mySet1 = new Set();
            this.product[prodCategory[k]] = mySet1.add(products[k])
          }
        }
        // console.log(this.product)
        for (let k in this.product) {
          let inner_parent1: any = {};
          inner_parent1['name'] = k;
          let child_array1 = [];
          // console.log(this.product[k])
          for (let l in Array.from(this.product[k].values())) {
            let inner_child1: any = {};
            inner_child1['name'] = Array.from(this.product[k].values())[l];
            child_array1.push(inner_child1)
          }
          inner_parent1['children'] = child_array1
          this.productOutput.push(inner_parent1)
        }
        this.dataSourceProducts.data = this.productOutput;
        console.log(this.arrProdCategory);
        // console.log(productOutput);
        // console.log(this.product);

      });

  }
  getLevel = (node: TodoItemFlatNode) => node.level;

  isExpandable = (node: TodoItemFlatNode) => node.expandable;


  hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

  getLevelLocation = (node: TodoItemLocationFlatNode) => node.level;

  isExpandableLocation = (node: TodoItemLocationFlatNode) => node.expandable;


  hasChildLocation = (_: number, _nodeData: TodoItemLocationFlatNode) => _nodeData.expandable;
  // hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

  todoLeafItemSelectionToggle(node: TodoItemFlatNode) {
    this.checklistSelection.toggle(node);
  }

  todoLeafItemSelectionLocationToggle(node: TodoItemLocationFlatNode) {
    this.checkListSelection.toggle(node);
  }
  descendantsAllSelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.length > 0 && descendants.every(child => {
      return this.checklistSelection.isSelected(child);
    });
    return descAllSelected;
  }
  descendantsAllSelectedLocation(node: TodoItemLocationFlatNode): boolean {
    const descendantsLocation = this.treeControlLocation.getDescendants(node);
    const descAllSelectedLocation = descendantsLocation.length > 0 && descendantsLocation.every(child => {
      return this.checkListSelection.isSelected(child);
    });
    return descAllSelectedLocation;
  }
  descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }
  descendantsPartiallySelectedLocation(node: TodoItemLocationFlatNode): boolean {
    const descendantsLocation = this.treeControlLocation.getDescendants(node);
    const resultLocation = descendantsLocation.some(child => this.checkListSelection.isSelected(child));
    return resultLocation && !this.descendantsAllSelectedLocation(node);
  }
  todoItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);
    descendants.forEach(child => this.checklistSelection.isSelected(child));
  }
  todoItemSelectionToggleLocation(node: TodoItemLocationFlatNode): void {
    this.checkListSelection.toggle(node);
    const descendantsLocation = this.treeControlLocation.getDescendants(node);
    this.checkListSelection.isSelected(node)
      ? this.checkListSelection.select(...descendantsLocation)
      : this.checkListSelection.deselect(...descendantsLocation);
    descendantsLocation.forEach(child => this.checkListSelection.isSelected(child));
  }
  onTreeNodeSelect(event: KeyboardEvent): void {
    // alert('node selected');
    this.selectPanel._handleKeydown(event);
  }
  ngOnInit() {
    this.dataSourceProducts.data = this.locationOutput;
    this.dataSourceLocation.data = this.productOutput;

  }
  onFormSubmit(formValue: any) {
    this.isSubmitted = true;
    console.log(formValue);
    if (this.sliderForm.valid) {
      console.log('form is valid')
      // this.router.navigate(['/planning']);
    }
    else {
      console.log('error');
    }
  }
  isFormSubmit(formvalue: any) {

    this.isSubmitted = true;
    console.log(formvalue);
    console.log(this.weeks, this.locationList, this.products, this.startDate, this.endDate);
    if (this.mainForm.invalid) {
      console.log("invalid")
      return;
    }
    else {
      console.log("valid")
    }
  }

}
