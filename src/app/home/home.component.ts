import { Component, OnInit, ViewChild } from '@angular/core';
import { menuItems } from '../sideMenu';

import { assets } from '../assetsPath';
import { MatAccordion } from '@angular/material/expansion';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  assetsPath: any = assets;
  menuOptions: any = JSON.parse(JSON.stringify(menuItems));
  isMenuOpen: boolean = false;
  // @ViewChild(MatAccordion) accordion: MatAccordion;
  username: any;
  profile: any;
  constructor(public router: Router) { }

  ngOnInit() {
    this.username = localStorage.getItem('currentUser')
    if (this.username !== "") {
      const match = this.username.match(/\b(\w)/g);
      this.profile = match.join('').toUpperCase();

    }
  }
  // toggle menu handler
  toggleMenu(isOpened?: boolean) {
    if (isOpened) {
      this.isMenuOpen = true;
    } else {
      this.isMenuOpen = !this.isMenuOpen;
      // this.accordion.closeAll();
    }
  }

}

