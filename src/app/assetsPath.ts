export const assets = {
    appLogoImg: 'assets/Images/cbtlogo.png',
    appLogoWhiteImg: 'assets/images/tpologo.png',
    clusterBgImg: 'assets/Images/cluster-bg.png',




    // material icons
    closeIcon: 'close',
    arrowForwardIcon: 'arrow_forward',
    rightArrowIcon: 'arrow_right_alt',
    menuOpenIcon: 'menu_open',
    stopCircleIcon: 'stop_circle',
    fullscreenIcon: 'fullscreen',
    saveAltIcon: 'save_alt',
    addIcon: 'add',
    factoryIcon: 'domain',
    clusterIcon: 'room'

};
